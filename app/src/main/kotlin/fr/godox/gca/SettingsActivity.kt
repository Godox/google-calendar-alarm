package fr.godox.gca

import android.Manifest.permission.SET_ALARM
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceFragmentCompat
import java.util.*
import kotlin.system.exitProcess

class SettingsActivity : AppCompatActivity() {

    lateinit var GCAlarmManager: GCAlarmManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }
        requestSetAlarmPermission {
            GCAlarmManager = GCAlarmManager(applicationContext).apply {
                setAlarm(9, Date().minutes + 1)
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun requestSetAlarmPermission(callback: () -> Unit) {
        if (ContextCompat.checkSelfPermission(applicationContext, SET_ALARM) != PackageManager.PERMISSION_GRANTED) {
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                if (!it) {
                    Toast.makeText(
                        applicationContext, R.string.no_permission_set_alarm, Toast.LENGTH_LONG
                    ).show()
                    exitProcess(0)
                }
            }
        }
        callback()
    }

}


class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}
