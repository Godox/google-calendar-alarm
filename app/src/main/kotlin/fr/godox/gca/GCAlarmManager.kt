package fr.godox.gca

import android.app.AlarmManager
import android.app.AlarmManager.RTC_WAKEUP
import android.content.Context
import java.util.*
import android.app.PendingIntent
import android.content.Intent
import android.os.SystemClock
import android.provider.AlarmClock
import android.provider.AlarmClock.*


class GCAlarmManager(val applicationContext: Context) {

    fun setAlarm(hours: Int, minutes: Int) {
        Intent(ACTION_SET_ALARM).apply {
            putExtra(EXTRA_HOUR, hours)
            putExtra(EXTRA_MINUTES, minutes)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                putExtra(EXTRA_MESSAGE, R.string.)
            applicationContext.startActivity(this)
        }
    }

}